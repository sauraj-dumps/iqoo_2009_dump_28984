#!/bin/bash

cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat system/system/framework/vivo-res.apk.* 2>/dev/null >> system/system/framework/vivo-res.apk
rm -f system/system/framework/vivo-res.apk.* 2>/dev/null
cat system/system/framework/framework-res.apk.* 2>/dev/null >> system/system/framework/framework-res.apk
rm -f system/system/framework/framework-res.apk.* 2>/dev/null
cat system/system/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/priv-app/Settings/Settings.apk
rm -f system/system/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system/system/priv-app/VivoBrowser/VivoBrowser.apk.* 2>/dev/null >> system/system/priv-app/VivoBrowser/VivoBrowser.apk
rm -f system/system/priv-app/VivoBrowser/VivoBrowser.apk.* 2>/dev/null
cat system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> system/system/product/priv-app/Velvet/Velvet.apk
rm -f system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat system/system/product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> system/system/product/priv-app/GmsCore/GmsCore.apk
rm -f system/system/product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat system/system/product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> system/system/product/app/WebViewGoogle/WebViewGoogle.apk
rm -f system/system/product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat system/system/product/app/YouTube/YouTube.apk.* 2>/dev/null >> system/system/product/app/YouTube/YouTube.apk
rm -f system/system/product/app/YouTube/YouTube.apk.* 2>/dev/null
cat system/system/product/app/Messages/Messages.apk.* 2>/dev/null >> system/system/product/app/Messages/Messages.apk
rm -f system/system/product/app/Messages/Messages.apk.* 2>/dev/null
cat system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat system/system/product/app/Photos/Photos.apk.* 2>/dev/null >> system/system/product/app/Photos/Photos.apk
rm -f system/system/product/app/Photos/Photos.apk.* 2>/dev/null
cat system/system/product/app/Maps/Maps.apk.* 2>/dev/null >> system/system/product/app/Maps/Maps.apk
rm -f system/system/product/app/Maps/Maps.apk.* 2>/dev/null
cat system/system/system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system/system/system_ext/apex/com.android.vndk.v30.apex
rm -f system/system/system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system/system/app/Gboard/Gboard.apk.* 2>/dev/null >> system/system/app/Gboard/Gboard.apk
rm -f system/system/app/Gboard/Gboard.apk.* 2>/dev/null
cat system/system/app/VivoCamera/VivoCamera.apk.* 2>/dev/null >> system/system/app/VivoCamera/VivoCamera.apk
rm -f system/system/app/VivoCamera/VivoCamera.apk.* 2>/dev/null
cat system/system/app/VivoGallery/VivoGallery.apk.* 2>/dev/null >> system/system/app/VivoGallery/VivoGallery.apk
rm -f system/system/app/VivoGallery/VivoGallery.apk.* 2>/dev/null
cat vendor_bootimg/06_dtbdump__L6Bkm$I~+."=.* 2>/dev/null >> vendor_bootimg/06_dtbdump__L6Bkm$I~+."=
rm -f vendor_bootimg/06_dtbdump__L6Bkm$I~+."=.* 2>/dev/null
cat .dtb.* 2>/dev/null >> .dtb
rm -f .dtb.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
