## qssi-user S SP1A.210513.004 compiler07272139 release-keys
- Manufacturer: vivo
- Platform: lahaina
- Codename: 2009
- Brand: iQOO
- Flavor: qssi-user
- Release Version: 11
- Id: SP1A.210513.004
- Incremental: compiler07272139
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: 440
- Fingerprint: iQOO/2009i/2009:11/RP1A.200720.012/compiler0727213850:user/release-keys
- OTA version: 
- Branch: qssi-user-S-SP1A.210513.004-compiler07272139-release-keys
- Repo: iqoo_2009_dump_28984


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
